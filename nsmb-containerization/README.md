# NSMB Containerization

## Access LRZ

```sh
ssh -Y lxlogin1.lrz.de -l ge42dac2
ssh -Y lxlogin2.lrz.de -l ge42dac2
ssh -Y lxlogin3.lrz.de -l ge42dac2
ssh -Y lxlogin4.lrz.de -l ge42dac2
```

## Build NSMB on LRZ

```sh
# available libraries
module load user_spack/23.1.0
spack --version
# 0.22.0.dev0 (should be over 0.18 for the correct Dockerfile generation)
module display intel-mpi/2019-intel
# 2019.12.320 (not existed in safe versions)
# using spack safe version 2019.10.317
module display intel-mkl/2020
# 2020.4.304 (existed but did not work with intel-mpi version)
# using spack safe version 2019.5.281
module display intel-oneapi-compilers/2021.4.0
# 2021.4.0
# using spack safe version 2021.3.0

# build legacy app
export LANG=en_US.utf8
export LC_ALL=en_US.utf8

unzip nsmb-idp.zip
cd nsmb6.09.24
rm lib/LINUX64/*
cd Install
./doinstall # see Backup/doinstall
vim doinstall.in # see Backup/doinstall.in
cd ../NSMB
make clean
make
cd
cp nsmb6.09.24/NSMB/nsmb6.09.24.mpi .
```

## Build NSMB in a container on a podman install linux host

```sh
podman build -t gitlab.lrz.de:5005/nfdi4ing/nsmb-container-registry/nsmb -f Dockerfile --format docker .
podman login gitlab.lrz.de:5005/nfdi4ing/nsmb-container-registry
podman push gitlab.lrz.de:5005/nfdi4ing/nsmb-container-registry/nsmb
```

## Run a single NSMB container on LRZ using CharlieCloud

Prerequisites
- Test data should be available on $SCRATCH/test-nsmb

```sh
module load charliecloud/0.30
ch-image pull --auth -s $SCRATCH/charliecloud-storage gitlab.lrz.de:5005/nfdi4ing/nsmb-container-registry/nsmb-container
ch-image list -s $SCRATCH/charliecloud-storage


export IMAGE=$SCRATCH/charliecloud-storage/img/yigitpolat%nsmb-container
ch-run -w --no-home --cd=/opt/nsmb-data/test-nsmb/ --set-env=$IMAGE/ch/environment --set-env=I_MPI_HYDRA_BOOTSTRAP=ssh --set-env=I_MPI_HYDRA_IFACE=ib0 -b $SCRATCH/test-nsmb:/opt/nsmb-data/test-nsmb $IMAGE -- ./nsmb6.09.24.mpi
```

## Run multiple NSMB container in parallel on LRZ using CharlieCloud and Slrum

Prerequisites
- Test data should be available on $SCRATCH/test-nsmb
- Create s script containing below code.

```sh
#!/bin/bash
#SBATCH -J {partition}_n{n}_tpn{tpn}
#SBATCH -o ./%x.%j.%N.out
#SBATCH -D ./
#SBATCH --get-user-env
#SBATCH --clusters={cluster}
#SBATCH --partition={partition}
#SBATCH --nodes={n}
#SBATCH --ntasks-per-node={tpn}
#SBATCH --mail-type=begin,end
#SBATCH --mail-user=yigit.polat@tum.de
#SBATCH --export=END
#SBATCH --time={time}

module load slurm_setup
module load charliecloud/0.30
export IMAGE=$SCRATCH/charliecloud-storage/img/yigitpolat%nsmb-container+centos
time mpiexec -n {mpi_tasks}  ch-run -w --no-home --cd=/opt/nsmb-data/test-nsmb/ --set-env=$IMAGE/ch/environment --set-env=I_MPI_HYDRA_BOOTSTRAP=ssh --set-env=I_MPI_HYDRA_IFACE=ib0 -b $SCRATCH/test-nsmb:/opt/nsmb-data/test-nsmb $IMAGE -- ./nsmb6.09.24.mpi
```

```
sbatch run_nsmb_container.sh
```


## Bonus: Run NSMB legacy way in parallel on LRZ using Slurm

Prerequisites
- Test data should be available on $SCRATCH/test-nsmb-legacy
- Legacy compiled NSMB binary should be placed $SCRATCH/test-nsmb-legacy

```sh
#!/bin/bash
#SBATCH -J {partition}_n{n}_tpn{tpn}
#SBATCH -o ./%x.%j.%N.out
#SBATCH -D ./
#SBATCH --get-user-env
#SBATCH --clusters={cluster}
#SBATCH --partition={partition}
##SBATCH --qos=cm2_std
#SBATCH --nodes={n}
#SBATCH --ntasks-per-node={tpn}
#SBATCH --mail-type=begin,end
#SBATCH --mail-user=hakan.acundas@tum.de
#SBATCH --export=END
#SBATCH --time={time}

module load slurm_setup
cd $SCRATCH/test-nsmb-legacy
time mpirun -n {mpi_tasks} ./nsmb6.09.24.mpi
```


