#
# Compilation rules
#
.SUFFIXES:
.SUFFIXES: .o .c .C .F .F90 .f90 .M4 .f .FL .h
#
.c.o :
	$(CC)  $(CFLAGS) -I$(INCLUDE) -c $*.c
########        ar ruv $(LIBNSMB) $*.o
.C.o :
	grep . $*.C > $*.c
	$(CC)  $(CFLAGS) $(DEFS) -I$(INCLUDE) -c $*.c
	-if (test $(CLEAN) = yes) then  rm -f $*.c ; fi
########        ar ruv $(LIBNSMB) $*.o
#
.f.o:
	$(F77) $(FFLAGS)  -I$(MPI_INC) -I$(INCLUDE) -c $*.f
#       -mv $(@F) $*.o
########        ar ruv $(LIBNSMB) $*.o
#
.f90.o:
	$(F77) $(FFLAGS)  -I$(MPI_INC) -I$(INCLUDE) -c $*.f90
#       -mv $(@F) $*.o
########        ar ruv $(LIBNSMB) $*.o
#
.F.o:
	$(CPP) $(DEFS) -P $*.F > $*.M4
	-if test `grep -ls 'include(swap.inc)' $*.M4` ; then \
        stick $*.M4 > $*.m4 ; rm $*.M4 ; $(M4) -Uindex -Ueval -Ulen $*.m4 > $*.M4 ; rm $*.m4 ; \
        fi
	$(M4) -Uindex -Ueval -Ulen -Uinclude -Uformat $*.M4 | awk -f $(INSTALL)/makefile.awk -  > $*.f
	$(F77) $(FFLAGS)  -I$(MPI_INC) -I$(INCLUDE) -c $*.f
#       -mv $(@F) $*.o
	-if (test $(CLEAN) = yes) then  rm -f $*.f $*.M4 ; fi
########        ar ruv $(LIBNSMB) $*.o
.F90.o:
	$(CPP) $(DEFS) -P $*.F90 > $*.M4
	-if test `grep -ls 'include(swap.inc)' $*.M4` ; then \
        stick $*.M4 > $*.m4 ; rm $*.M4 ; $(M4) -Uindex -Ueval -Ulen $*.m4 > $*.M4 ; rm $*.m4 ; \
        fi
	$(M4) -Uindex -Ueval -Ulen -Uinclude -Uformat $*.M4 | awk -f $(INSTALL)/makefile.awk -  > $*.f90
	$(F77) $(FFLAGS)  -I$(MPI_INC) -I$(INCLUDE) -c $*.f90
#       -mv $(@F) $*.o
	-if (test $(CLEAN) = yes) then  rm -f $*.f90 $*.M4 ; fi
########        ar ruv $(LIBNSMB) $*.o
