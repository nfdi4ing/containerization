#! /bin/bash
slurm_job=$1
n=$2
tpn=$3
mpi_tasks=$4
cluster=$5
partition=$6
time=$7
modified_script="${slurm_job%.sh}_n${n}_tpn${tpn}_mpi${mpi_tasks}_${partition}.sh"

sed -e "s/{n}/$n/g" \
    -e "s/{tpn}/$tpn/g" \
    -e "s/{cluster}/$cluster/g" \
    -e "s/{partition}/$partition/g" \
    -e "s/{mpi_tasks}/$mpi_tasks/g" \
    -e "s/{time}/$time/g" \
    "$slurm_job" > "$modified_script"

echo "Modified script created: $modified_script"
sbatch "$modified_script"