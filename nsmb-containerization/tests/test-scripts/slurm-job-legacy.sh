#!/bin/bash
#SBATCH -J {partition}_n{n}_tpn{tpn}_mpi{mpi_tasks}
#SBATCH -o ./%x.%j.%N.out
#SBATCH -D ./
#SBATCH --get-user-env
#SBATCH --clusters={cluster}
#SBATCH --partition={partition}
#SBATCH --nodes={n}
#SBATCH --ntasks-per-node={tpn}
#SBATCH --mail-type=begin,end
#SBATCH --mail-user=hakan.acundas@tum.de
#SBATCH --export=END
#SBATCH --time={time}

module load slurm_setup
cd $SCRATCH/test-nsmb-legacy
mpirun -n {mpi_tasks} ./nsmb6.09.24.mpi