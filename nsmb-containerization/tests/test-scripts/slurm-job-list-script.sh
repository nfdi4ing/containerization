#! /bin/bash
slurm_job=$1
min_n=$2
max_n=$3
tpn=$4
mpi_tasks=$5
cluster=$6
partition=$7
time=$8

for ((n = min_n; n <= max_n; n++)); do
    modified_script="${slurm_job%.sh}_n${n}_tpn${tpn}_mpi${mpi_tasks}_${partition}.sh"
    sed -e "s/{n}/$n/g" \
            -e "s/{tpn}/$tpn/g" \
            -e "s/{cluster}/$cluster/g" \
            -e "s/{partition}/$partition/g" \
            -e "s/{mpi_tasks}/$mpi_tasks/g" \
            -e "s/{time}/$time/g" \
            "$slurm_job" > "$modified_script"

    echo "Modified script created: $modified_script"
    sbatch "$modified_script"
done