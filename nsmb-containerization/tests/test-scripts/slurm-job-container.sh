#!/bin/bash
#SBATCH -J {partition}_n{n}_tpn{tpn}_mpi{mpi_tasks}
#SBATCH -o ./%x.%j.%N.out
#SBATCH -D ./
#SBATCH --get-user-env
#SBATCH --clusters={cluster}
#SBATCH --partition={partition}
##SBATCH --qos=cm2_std
#SBATCH --nodes={n}
#SBATCH --ntasks-per-node={tpn}
#SBATCH --mail-type=begin,end
#SBATCH --mail-user=hakan.acundas@tum.de
#SBATCH --export=END
#SBATCH --time={time}

module load slurm_setup

module load charliecloud/0.30

export IMAGE=$SCRATCH/charliecloud-storage/img/yigitpolat%nsmb-container+centos-minimal

time ch-run -w --no-home --unset-env='*' --set-env=$IMAGE/ch/environment -b $SCRATCH/test-nsmb:/opt/nsmb-data/test-nsmb $IMAGE -- mpirun -n {mpi_tasks} -wdir /opt/nsmb-data/test-nsmb /opt/nsmb-data/nsmb6.09.24.mpi