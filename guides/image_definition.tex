\documentclass[a4paper]{article}

\usepackage[pages=all, color=black, position={current page.south}, placement=bottom, scale=1, opacity=1, vshift=5mm]{background}

\usepackage[margin=1in]{geometry} % full-width

% AMS Packages
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}

% Unicode
\usepackage[utf8]{inputenc}
\usepackage{hyperref}

\usepackage{graphicx, color}
\graphicspath{{fig/}}

\usepackage{color}
\usepackage{listings}
\usepackage{hyperref}

\lstdefinelanguage{Dockerfile}
{
  morekeywords={FROM, RUN, CMD, LABEL, MAINTAINER, EXPOSE, ENV, ADD, COPY,
    ENTRYPOINT, VOLUME, USER, WORKDIR, ARG, ONBUILD, STOPSIGNAL, HEALTHCHECK,
    SHELL},
  morecomment=[l]{\#},
  morestring=[b]"
}

\lstdefinelanguage{singularity}
{	
	alsoletter={\%}
	keywords={},
	morekeywords={Bootstrap, From, Stage},
	keywords=[2]{\%post, \%files, \%environment, \%runscript, \%startscript},
	morecomment=[l]{\#},
	morestring=[b]"
}

\lstalias{apptainer}{singularity}

\lstset{
    columns=flexible,
    keepspaces=true,
    showstringspaces=false,
    basicstyle=\ttfamily,
    commentstyle=\color{gray},
    keywordstyle=\color{purple},
	keywordstyle=[2]\color{blue},
    stringstyle=\color{green}
}

% Author info
\title{Starter's guide to containerizing programs on HPC clusters}
\author{Alexander Schindler}

\begin{document}
	\maketitle

	\section{Basics of Creating Containers}

	Before we take a deeper look at how to actually create containers let's first get an understanding of what containers are 
	and why we would want to use them on HPC clusters. \newline
	Containers are a lightweight virtualization technique. They exist in many forms (the most popular one is Docker but, 
	as we will see, this is currently not what we are working with) yet all share some common principles. They abstract parts 
	of the system and by doing so enable portability to a certain extent. The ideal case would be to create a container image on one 
	system (e.g. an LRZ cluster). The image is pushed to a repository and downloaded at another system. The goal is to run it there under exactly the 
	same conditions, yielding the same results and requiring zero additional effort of adjusting, compiling and running the program.

	In reality this cannot be achieved completely, but this project puts an effort to get close to this goal.
	It is important to understand what virtualization means in this context. This is best done 
	by comparing it to a virtual machine (VM). The VM emulates even the underlying hardware within some kind of hypervisor. Everything 
	that runs in a VM is thus completely isolated from the system that runs the hypervisor, it sees the emulated architecture as if it 
	were real. A container on the other hand only virtualizes parts of the system (never the hardware). At the very least it will share the kernel with 
	the host system. Then, depending on configuration, the container can e.g. virtualize the file system, user IDs, network access etc. 
	(we call those domains \textit{namespaces}). This means whatever program runs in the container will see a fresh version of these 
	resources, distinct from the host system. For instance, if we virtualze the file system (which is what most containerization software 
	will do by default) we will have a fresh view of user space programs and can set up a clean version of the system.
	
	It is important 
	to notice that one should never assume any system isolation or ecurity guarantees at any rate. The virtualization is only providing
	convenience and portability. Usually one has quite a lot of options to configure and customize containers. For the purposes regarded 
	in this project however, a rather basic usage of containers is sufficient.

	\section{Purpose of this Guide}

	This guide helps the reader in setting up any kind of relevant container for use on an HPC cluster. This includes understanding 
	the differences of the systems (currently LRZ and JSC, more to follow) and how to write the container source code, build the image and 
	submit it to the cluster's batch system for the different setups. The different container systems that are being used also support 
	various formats of input files and even pre-built images that can be run. However, there seems to be no possibility to utilize 
	that on the considered HPC clusters as of now. Thus one also needs to understand how to reengineer a container to run it on another system. 

	\section{Spack}

	This section provides some useful information about Spack and how to use it. We will use Spack \textit{inside} our 
	containers for a couple of reasons. First of all, Spack is a package manager. There are other package managers out there in the Linux 
	world: apt, dnf, pacman, nix to name a few (maybe some sound familiar). Simply put, a package manager is a tool for installing and 
	managing software in a convenient way. It provides unified update mechanisms and, most importantly, takes care of all kinds of 
	dependencies.
	
	Spack in particular is a package manager that tends to the needs of HPC users. It allows for several versions of 
	any software to be installed simultaneously on a high level of granularity. You can choose to install a certain piece of software 
	of a specified version, compiled with specified flags and even do the same for its dependencies if necessary. It won't conflict 
	with other versions of that same software. A system user can be certain of what programs and libraries are being used in exactly 
	what version and configuration independent from any system updates. More importantly, this is also independent of the system itself, 
	hence we gain portability.

	By using Spack, no other mechanisms for configuring software is necessary (i.e. loading modules) inside the container. Instead, 
	everything is managed through a Spack environment, which needs to be configured by the user. To do so, a \texttt{spack.yaml} file 
	is generated that describes the exact specification. This can be complex; the provided templates however contain most necessary 
	steps already. The only thing the user must adjust is the exact software stack that is desired. This is placed in the \texttt{specs} 
	section of the YAML file (the templates have comments to indicate where this should be done). At this point it is necessary to know 
	the syntax of specifying software for Spack. Basic usage is as follows:
	
	\begin{lstlisting}
		name@version

		# example:
		intel-oneapi-compilers@2021.3.0

		# extended example (from the official doc)
		mpileaks @1.2:1.4 %gcc@4.7.5 +debug -qt target=x86_64 \
			^callpath @1.1 %gcc@4.7.2
	\end{lstlisting}

	The example shows the most likely use case of simply a specific version being selected. Everything else will be done automatically 
	depending on the system. The extended example shows what \textit{can} be done. This way one could guarantee the exact same software 
	is built across all systems at any time. However, there are drawbacks. In the HPC environments a lot of things are quite 
	hardware dependent, usually because performance is the number one goal. This is conflicting with the idea of portability accross 
	platforms; a problem this project tries to assess and mitigate as much as possible. An example is stated in the official Spack 
	documentation\footnote{Spack documentation: \url{https://spack.readthedocs.io/en/latest/getting_started.html}}: MPI frameworks are 
	to be used from pre-built images. Otherwise they might not yield optimal performance or not work at all. Thus it is better to choose a 
	version of such a framework that is actually available on the system. Finding an optimal solution and manage portability is still a 
	work in progress. \newline
	If you want to understand the extended example in detail, have a look at the documentation's spec 
	section\footnote{Spack specs: \url{https://spack.readthedocs.io/en/latest/basic_usage.html\#specs-dependencies}}.

	\section{Containers for Charliecloud on the LRZ}

	The LRZ uses charliecloud as their containerization software. Here is a quick overview of charlieclouds features as well as the LRZ 
	configuration:

	\begin{itemize}
		\item No root permission is necessary to build images
		\item Standard Dockerfiles can be used (although there is no full syntax support)
		\item Charliecloud needs to be loaded as a module
	\end{itemize}

	For defining your container you can write a Dockerfile. This is the most popular format for defining containers, hence it is easy to 
	find resources on the internet. As a base image you want to use the Spack image available from the Docker Hub, which is as simple as
	using the \texttt{FROM} keyword like this:

	\begin{lstlisting}[language=Dockerfile]
		FROM spack/ubuntu-bionic:latest
	\end{lstlisting}

	Then you write your setup code as if you would work on a fresh system that has spack available: install the packages you need, 
	copy resources from outside the image, configure the environment, build your custom software, etc.. If you need help with that, 
	Appendix \ref{appendix:lrz} shows a template with some useful comments about how to set everything up.
	Also, the official documentation\footnote{
		Dockerfile reference: \url{https://docs.docker.com/engine/reference/builder/}
	} for 
	the format is really helpful. You should especially have a look at the RUN, COPY, ADD and ENV commands. Unfortunately some commands 
	are not supported. There seems to be no possibility to create a default command for container startup; you will have to invoke it at 
	container startup.

	The most outstanding feature of charliecloud is that no root permission is required to build and run containers. It offers two modes of 
	unpriviliged builds; for our simple tests the default mode was sufficient (which is activated via \texttt{--force}). This makes it quite 
	easy for users on HPC clusters, since root is generally not provided. Without worrying about details, this means you can easily build 
	your images on a login node of the lrz like this:

	\begin{lstlisting}[language=bash]
		ch-image build -t <image name (tag)> -f <Dockerfile> \
		-s $SCRATCH/<storage directory> --force <context directory>
	\end{lstlisting}

	The tag is like a name with which charliecloud can find the image in its database. The context directory is the directory the builder sees 
	as its base, i.e. 
	the base for \texttt{COPY} instructions. The storage directory is where the image is first built.
	It requires a lot of space (our simple test container needed about 19GB), thus we recommend choosing a location on \texttt{\$SCRATCH}. 
	At this location you can actually inspect the image,
	as charliecloud builds are accessible from the host's filesystem. It is possible to run the container from within it, however, the 
	official documentation\footnote{Charliecloud reference: \url{https://hpc.github.io/charliecloud/}} discourages this. The recommended 
	workflow requires the image to be added to charlieclouds database, which doesn't seem to work (this should happen upon a successful build of 
	an image but doesn't). You can 
	find the container at \texttt{<context directory>/img/<image name>}. 
	
	Finally, for submitting your job to slurm you can use sbatch 
	scripts pretty much the same way you would for a program that is built on the host. The only difference is that the 
	actual call to the target program is replaced by a call to \texttt{ch-run} like this:

	\begin{lstlisting}[language=bash]
		ch-run  <path to image> -- <command>

		# feed this to mpiexec for parallelization:
		mpiexec -n <tasks> ch-run  <path to image> -- <command>
	\end{lstlisting}

	It may be required to add certain flags for \texttt{ch-run}. Especially the \texttt{-w} flag for mounting the image in read-write mode,
	\texttt{-b} for bind-mounting directories into the container and \texttt{-c} for changing the working directory in the container are 
	gonna be useful. The 
	command is then invoked within the container. Note that the base is the directory that is marked as the working directory in the 
	container; make sure that the command is e.g. a correct path to your executable.

	Images can be pushed to special repositories (e.g. gitlab and github have such features). This makes it easy to share pre-built images 
	if they are not stored in shared directories. 
	% Add more info here

	A template Dockerfile will be provided (see appendix \ref{appendix:lrz}), which you can use for your own projects and adjust to your needs. 
	Also have a look at 
	section~\ref{section:tips} for some handy tips and tricks that you might want to use.

	\section{Containers for Apptainer on the JSC}

	The JSC uses apptainer as their containerization software. Here is a quick overview of apptainers's features as well as the jsc 
	configuration:

	\begin{itemize}
		\item Image builds need root permission; Fakeroot builds seem to fail. Thus the image needs to be built externally and 
		then transferred
		\item Apptainer has its own format for specifying containers (some scripts help to transform a Dockerfile)
		\item The user must be part of the container group. This can be requested in the JuDoor portal
	\end{itemize}

	Despite Apptainer having a fakeroot-feature available, it seems to not work reliably (in our test it didn't work at all). 
	Chaliecloud does a way better job at keeping consistency for "simulated" system calls that would require root permission. As a 
	consequence, there currently seems to be no way to build an image on the cluster itself (You are invited to try with the option 
	\texttt{--fakeroot}, you would save a lot of time if this works). A second option, a build pipeline offered on JSC, is 
	unmaintained and thus discouraged to use in the JSC documentation\footnote{JSC documentation: \url{https://apps.fz-juelich.de/jsc/hps/juwels/index.html}} 
	(We would not recommend it either). This leaves us no choice but to build it externally. 
	First of all, apptainer needs to be available on this external system. On top of that, root permission is required for the 
	build. Until a server-sided solution is found, this means you would have to build the image on your personal computer. 
	Then you need to transfer it to the cluster via scp (probably to the system where you have stored the keys for jsc first). 

	Apptainer used to be called Singularity. To specify an image, you will need to write a Singularity definition file (typically .def). 
	The definition file is divided in sections that dictate what to do and when to run the shell code that they contain. Initially, you 
	start with a system that has spack available; thus your first lines should be those:

	\begin{lstlisting}[language=singularity]
		Bootstrap: docker
		From: spack/ubuntu-bionic
		Stage: spython-base	
	\end{lstlisting}

	The rest of the file needs to contain the following: A \texttt{\%files} section for copying files into the container in the form of 
	\texttt{<source(host)> <destination(container)>}. A \texttt{\%post} section containing everything that needs to be done within the 
	container when it is being built. This also includes setting environment variables that are necessary e.g. during compilations. 
	The \texttt{\%environment} section will set environment variables that are available during container runtime. You might need to 
	repeat some of the environment variables from the \texttt{\%post} section here. Finally, the \texttt{\%runscript} section contains 
	code that is executing upon container startup. An example project can be found in the Apptainer directory of the project repository. 
	For more details and more features, refer to the official Apptainer 
	documentation\footnote{Apptainer Documentation: \url{https://apptainer.org/docs/user/latest/}}. A template will be provided (see appendix 
	\ref{appendix:jsc}) that 
	should make it a lot easier to get started. If you want to see our test example, have a look at the Apptainer folder in the repository. 
	Also refer to section \ref{section:tips} for some more useful information. Once you have 
	the definition file ready, build a container on your (local) machine as follows:

	\begin{lstlisting}[language=bash]
		apptainer build <path to image>.sif <path to definition file>
	\end{lstlisting}

	This will result in an executable \texttt{.sif} file at your specified location. Once you have transferred the image to the cluster, 
	you can write a batch script for submission almost the same way you would if you have your program ready on the host. 
	Note that \texttt{mpiexec} is not available and srun should be used instead. Your script should contain the correct sbatch parameters. 
	This includes e.g. on which cluster to run the container and your mail address for notification. You can either set this as \texttt{srun} 
	options or in a comment style, increasing readability. The example project contains a shell script for reference (Apptainer directory in 
	the project repository). 
	The command to be invoked by \texttt{srun} then is as follows:

	\begin{lstlisting}[language=bash]
		apptainer run --no-home <path to image>

		# for parallelization use srun
		srun -n <tasks> apptainer run --no-home <path to image>
	\end{lstlisting}

	The \texttt{--no-home} option is necessary because Apptainer will bind mount your home directory into the container by default. If 
	you want this behavior you can leave it out. There are a few more things worth mentioning. The \texttt{--bind} option can be used 
	for bind mounting directories into the container at startup time. This can be useful to access input data from within the container. 
	Another interesting option is \texttt{--writable} to set the container's file system in read-write mode (it is read only by default).
	You also may have noticed that the invokation of a program to be run in the container is missing. The reason for this is that you 
	can use the \texttt{\%runscript} section to set a default command to be run. If, for some reason, you don't want to use this section 
	you can add the option \texttt{--app} to set an executable to be run in the container. 

	\section{Transforming Images}

	There are ways of transforming images but we didn't find a satisfying solution yet. On the JSC we were not able to pull images from 
	an external repository (which would be the easiest solution). There are two reasons for this: the servers in Juelich are quite 
	restrictive and there were issues connecting to the repository server. It is not quite clear if charliecloud images would be 
	supported at all. Apptainer can use Docker as well as OCI (Open Container Initiative) containers. Charliecloud could support 
	Docker and Podman containers. There would be a possibility to transform either an Apptainer or Charliecloud container into a 
	Docker container vice versa. This approach would require docker on the machines that build the respective containers, which 
	excludes the HPC clusters. That's why this approach hasn't been tested out.

	Currently, the only way to transform images is by transforming the specifications. This is obviously not ideal, but better than 
	having to manually rewrite everything. The tool that helps with this is called \texttt{spython}. It is a script and library for 
	python to interact with Singularity (now Apptainer). It may be outdated, the definition file format seems rather stable however. It 
	can transform a Dockerfile into a Singularity Definition file and the other way round. You can install it with \texttt{pip install sypthon} 
	or \texttt{pip3 install spython} (depending where your \texttt{pip3} can be found) on 
	you local machine. Then you can use it from you command line like this:

	\begin{lstlisting}[language=bash]
		spython recipe <Source> <Transformed>
	\end{lstlisting}

	This will create a new file \texttt{Transformed} with the target format. A handy auto-detection for formats is being used, however 
	you can specify them manually. You can read about how to do that and how formats are detected in the spython recipe 
	documentation\footnote{spython recipe: \url{https://singularityhub.github.io/singularity-cli/recipes}}. You will still need to 
	modify some things to make it work though. The provided templates will help as well.

	\section{General Tips and Tricks}
	\label{section:tips}

	\subsection*{Soft Links}
	Soft Links are a tool that can help you load large files which are needed at runtime into the container without having to copy 
	them. It needs to be combined with a bind-mount into the container. A soft link is essentially a file that contains a path to another file (or directory) and 
	gets a special treatment by the file system. One noteworthy thing about softlinks is that they can also point to non-existing files, 
	because they are completely independent of the object they point to. It makes them error prone in a way, but we make use of this by letting 
	it point to a file that only exists at a future point in time.

	First, bind-mount the directory that contains the files to be made available in the container at container startup time. Read in the 
	specific sections for any HPC system how this is done (it is usually an option for the startup command). 
	Sometimes this already is enough, as the path in the 
	container this directory should be placed can be specified(no softlink needed). Sometimes there may be restrictions, 
	however, and the input file should be located at a very specific 
	place in the file system without mounting it at that point. What can be done instead is place a softlink at the position where e.g. 
	the input file should be at. It point to a file in the bind-mounted directory. The interesting thing is that it can be created 
	when building the image (e.g. in the container specification file), even though the directory will be mounted at a later 
	time. The basic syntax is this:

	\begin{lstlisting}[language=bash]
		ln -s <filename> <linkname>
	\end{lstlisting}

	More on softlinks (they are also called symbolic links) can be found on the \texttt{ln} 
	manpages\footnote{ln manpage: \url{https://www.man7.org/linux/man-pages/man1/ln.1.html}} and the \texttt{symlink} 
	manpages\footnote{symlink manpage: \url{https://www.man7.org/linux/man-pages/man7/symlink.7.html}}.

	\subsection*{Spack Container}

	Spack can actually generate Dockerfiles and Singularity definition files from a \texttt{spack.yaml}. This may be useful if there is 
	a large Spack specification. If spack is available on your system, a Dockerfile can be generated from any Spack environment by calling 

	\begin{lstlisting}[language=bash]
		spack containerize > <Filename>
	\end{lstlisting}

	in the directory the \texttt{spack.yaml} file is located. For creating Singularity definition files you will need to adjust the 
	\texttt{spack.yaml} by adding a configuration under the \texttt{container} keyword. More information on how to do this and much 
	more can be found in the container image section of the Spack 
	documentation\footnote{spack containers: \url{https://spack.readthedocs.io/en/latest/containers.html}}.

	\subsection*{Image Size and Cleanup}

	So far, for our experiments, we only considered getting everything to work and testing things out. Efficiency of the process was 
	not the main goal so far. It should be noted that the container images as we use them are \textit{really} big, much bigger than 
	they need to be (some data will follow in the future). If that is unacceptable to you, you should put an effort in keeping your 
	container clean. This means remove any 
	artifacts from build processes, uninstall packages that were only needed for building and so on. At least Dockerfiles have a multi 
	stage build feature which can be utilized for convenience. Also, sadly no official spack image based on Alpine Linux exists (which 
	would save a bit more space). As the project moves on this topic might shift more into focus and the guide and templates will be 
	adapted to consider creating minimal images.

	\section{Future Project Goals}

	We are at a rather early stage of exploring capabilities and usability of containers on HPC systems. Future work might include extending 
	this guide for other HPC centers, optimizing performance of containers, streamlining workflows for creation and transformation of 
	images e.g. through automation scripts, and working together with admins to improve the usability of their systems regarding 
	containers.

	\clearpage
	\appendix
	
	\section{LRZ Template}
	\label{appendix:lrz}

	\lstinputlisting[language=Dockerfile,breaklines=true,postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space}]{../templates/lrz_template}

	\clearpage
	\section{JSC Template}
	\label{appendix:jsc}

	\lstinputlisting[language=Singularity,breaklines=true,postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space}]{../templates/jsc_template}
	
\end{document}

