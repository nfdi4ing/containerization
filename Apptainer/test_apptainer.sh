#!/bin/bash

#SBATCH --output=out_test.%j.out
#SBATCH --error=err_test.%j.err
#SBATCH --nodes=10
#SBATCH --ntasks-per-node=28
#SBATCH --mail-type=end
#SBATCH --mail-user=alex.schindler@tum.de
#SBATCH --export=NONE
#SBATCH --time=4:00:00
#SBATCH --partition=batch
#SBATCH --account=nfdi4ingjuwels

time srun -n 280 apptainer run --bind $(pwd)/data:/DAT --no-home hpl.sif

