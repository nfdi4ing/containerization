#!/bin/bash

for i in {1..1}
do
	echo "sleep 300" >> simple_eco
	echo "mpiexec -n 112 ch-run --set-env=$(pwd)/../spack_uni/ch/environment -b $(pwd)/ECOGEN:/mnt/0 --cd /mnt/0 ../spack_uni -- ./ECOGEN" >> simple_eco
	echo "sleep 300" >> simple_eco
	echo "mpiexec -n 112 ch-run --set-env=$(pwd)/../spack_uni/ch/environment -b $(pwd)/ECOGEN:/mnt/0 --cd /mnt/0 ../spack_uni -- ./ECOGEN" >> simple_eco
	echo "sleep 300" >> simple_eco
	echo "mpiexec -n 112 ch-run --set-env=$(pwd)/../spack_uni/ch/environment -b $(pwd)/ECOGEN:/mnt/0 --cd /mnt/0 ../spack_uni -- ./ECOGEN" >> simple_eco
        sbatch --output="out_4.%j" \
               --error="err_4.%j" \
               --time=12:00:00 \
               --clusters=tum_aer \
               --partition=tum_aer_large \
               --nodes=4 \
	       			 --mem=100000 \
               --ntasks-per-node=28 \
               -D ./ \
               simple_eco
        sed -i '$ d' simple_eco
        sed -i '$ d' simple_eco
        sed -i '$ d' simple_eco
        sed -i '$ d' simple_eco
        sed -i '$ d' simple_eco
        sed -i '$ d' simple_eco
done
