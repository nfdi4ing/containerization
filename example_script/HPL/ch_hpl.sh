#!/bin/bash

for i in {1..1}
do
	#NODE=$(( 2**i ))
	#SLURM_NTASKS=$(( 2**i * 28 ))
	echo "sleep 300" >> simple_hpl
	echo "time mpiexec -n 112 ch-run --set-env=$(pwd)/../spack_uni/ch/environment -b $(pwd)/mount/.:/mnt/0 --cd /mnt/0 ../spack_uni -- ./xhpl" >> simple_hpl
	echo "sleep 300" >> simple_hpl
	echo "time mpiexec -n 112 ch-run --set-env=$(pwd)/../spack_uni/ch/environment -b $(pwd)/mount/.:/mnt/0 --cd /mnt/0 ../spack_uni -- ./xhpl" >> simple_hpl
	echo "sleep 300" >> simple_hpl
	echo "time mpiexec -n 112 ch-run --set-env=$(pwd)/../spack_uni/ch/environment -b $(pwd)/mount/.:/mnt/0 --cd /mnt/0 ../spack_uni -- ./xhpl" >> simple_hpl
	sbatch --output="out_4.%j" \
	       --error="err_4.%j" \
	       --time=12:00:00 \
	       --clusters=tum_aer \
	       --partition=tum_aer_large \
	       --mem=90000 \
	       --nodes=4 \
	       --ntasks-per-node=28 \
	       -D ./ \
	       simple_hpl
	sed -i '$ d' simple_hpl
	sed -i '$ d' simple_hpl
	sed -i '$ d' simple_hpl
	sed -i '$ d' simple_hpl
	sed -i '$ d' simple_hpl
	sed -i '$ d' simple_hpl
done
