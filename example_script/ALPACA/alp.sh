#!/bin/bash

for i in {2..4}
do
        NODE=$(( 2**i ))
        SLURM_NTASKS=$(( 2**i * 28 ))
        echo "mpiexec -n $SLURM_NTASKS ./ALPACA_EXEC He_strong.xml" >> simple_alp
        sbatch --output="out_$NODE.%j" \
               --error="err_$NODE.%j" \
               --time=00:10:00 \
               --clusters=cm2 \
               --partition=cm2_std \
	       			 --qos=cm2_std \
               --nodes=$NODE \
               --ntasks-per-node=28 \
               -D ./ \
               simple_alp
        sed -i '$ d' simple_alp
done
