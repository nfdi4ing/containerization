#!/bin/bash

for i in {1..1}
do
	echo "sleep 300" >> simple_alp
	echo "mpiexec -n 112 ch-run --set-env=$(pwd)/../spack_uni/ch/environment -b $(pwd)/mount:/mnt/0 --cd /mnt/0 ../spack_uni -- ./ALPACA He_middle.xml" >> simple_alp
	echo "sleep 300" >> simple_alp
	echo "mpiexec -n 112 ch-run --set-env=$(pwd)/../spack_uni/ch/environment -b $(pwd)/mount:/mnt/0 --cd /mnt/0 ../spack_uni -- ./ALPACA He_middle.xml" >> simple_alp
	echo "sleep 300" >> simple_alp
	echo "mpiexec -n 112 ch-run --set-env=$(pwd)/../spack_uni/ch/environment -b $(pwd)/mount:/mnt/0 --cd /mnt/0 ../spack_uni -- ./ALPACA He_middle.xml" >> simple_alp
        sbatch --output="out_4.%j" \
               --error="err_4.%j" \
               --time=12:00:00 \
               --clusters=cm2 \
               --partition=cm2_std \
				       --qos=cm2_std \
				       --mem=53000 \
				       --nodes=4 \
               --ntasks-per-node=28 \
               -D ./ \
               simple_alp
        sed -i '$ d' simple_alp
        sed -i '$ d' simple_alp
        sed -i '$ d' simple_alp
        sed -i '$ d' simple_alp
        sed -i '$ d' simple_alp
        sed -i '$ d' simple_alp
done
