# Welcome to the GitLab Repository of Containerisation Workflow of CFD Platforms
## All files in this repository follow their own license (if applicable). Otherwise, other files are under GNU GPLv3 license. ##

### How to Start ###
Simply clone this directory with the submodules by
`git clone --recurse-submodules https://gitlab.lrz.de/nfdi4ing/containerization.git`
Note that HPL benchmark doesn't have repository, so it's directly added as a `.zip` (the Makefile is modified, please refer to `Containerization_of_CFD_workflow_Guide.pdf`).

### In Dockerfile folder ###
There are some pre-defined Dockerfiles. Modify it to your need before using them. When building the image, change the file name back to "Dockerfile" (ignore quotes) to let Docker find the default file to build the image.
- Dockerfile_LRZ: this environment installed is in the way that it's as close as possible to the LRZ Linux cluster (latest modules version are still the same up to 28.10.2021). The compiler used in the image is from Intel-OneAPI, on LRZ it uses the licensed Intel compiler (i.e., module load intel).
- Dockerfile_old_impi: this is an image template using older version on Intel related modules.
- Dockerfile_paraview: contains a ParaView 5.7.0 OSmesa version in the image. For this we don't need to install additional MPI modules, because ParaView uses its own MPI program.
- Dockerfile_mpich: the very first Dockerfile using MPICH to build ALPACA executable program.
#### working example folder ####
It has two versions of intel-mpi. If you clone this repository with submodules, you can run these Dockerfiles at root folder, so Docker build can find the softwares properly. `hpl-2.3.zip` needs to be extracted first.

The deprecated folder has other Dockerfiles just for reference.

### In example_script folder ###
There are three folders corresponding to three applications:
- ALPACA: contains two shell scripts for running vanilla and container tests. The ".pbs" file is an example of how to run the application on the PBS job queue system. simple_alp is the actual script to submit to the cluster. In the inputs folder, some examples are provided. Simply change the name in either the bash file or the input file itself to make the command match the input file name.
- ECOGEN: similar to above. To replace the input files, copy all of them to ```<ECOGEN_root>/libTests/referenceTestCases/PUEq/3D/shockBubble/heliumAir```. In the root folder, make sure the test of helium air is uncommented (i.e., it should look like ```<testCase>./libTests/referenceTestCases/PUEq/3D/shockBubble/heliumAir/</testCase>```).
- HPL: only contains the script for running the application in Charliecloud. An example input file is written in HPL.dat. For tuning parameters, check: [How to Run an Optimized HPL Linpack Benchmark](https://www.pugetsystems.com/labs/hpc/How-to-Run-an-Optimized-HPL-Linpack-Benchmark-on-AMD-Ryzen-Threadripper----2990WX-32-core-Performance-1291/)

### In results folder ###
It contains the plotting results showed in the workflow guide. The Python notebook `IDP_code.ipynb` could be used to reproduce the results.
The `IDP_slides.pdf` are used in the final presentation. After the Q&A slide there are some backup pages that have more information on optimisation.

### Further literature and guides ###
Handout: [Introduction to Containers & Application to AI at LRZ](https://collab.dvb.bayern/lrzpublic/files/10333516/10333518/1/1684601252667/handout.pdf) (Dufour / LRZ, 2023)
Slides: [Introduction to Containers & Application to AI at LRZ](https://doku.lrz.de/files/10333516/1078568775/3/1730129043540/Introduction_to_containers.pdf) (Dufour / LRZ, 2024)


### Acknowledgements ###
The authors would like to thank the Federal Government and the Heads of Government of the Länder, as well as the Joint Science Conference (GWK), for their funding and support within the framework of the NFDI4Ing consortium. Funded by the German Research Foundation (DFG) project number 442146713.

The authors gratefully acknowledge the Gauss Centre for Supercomputing e.V. (www.gauss-centre.eu) for funding this project by providing computing time through the John von Neumann Institute for Computing (NIC) on the GCS Supercomputer JUWELS at Jülich Supercomputing Centre (JSC).

The authors would also thank the Competence Network for Scientific High Performance Computing in Bavaria (KONWIHR) for their funding and support within a short term project.
